!$fcg start define
Tabs :
  "integer(4)"   : "i4"
  "integer(8)"   : "i8" 
  "real(4)"      : "r4" 
  "real(8)"      : "r8"
  "complex(4)"   : "c4" 
  "complex(8)"   : "c8"
  "logical"      : "l"
  "character(*)" : "ch"
!$fcg end define

!$fcg start generator
!$fcg start parameterize
T :
  - "integer(4)" 
  - "integer(8)" 
  - "real(4)" 
  - "real(8)"
  - "complex(4)"
  - "complex(8)"
  - "logical"
  - "character(*)"
!$fcg end parameterize
subroutine initilizer_${Tabs[${T}]}(target,val0)
  implicit none
  ${T}, intent(out)           :: target
  ${T}, intent(in), optional  :: val0
  !---------------------
  
  if ( present(val0) ) then
    target = val0
  else
    !$fcg start switch ${T}
    !$fcg case in ["integer(4)","integer(8)"]
    target = 0
    !$fcg case "real(4)"
    target = 0.0e-00
    !$fcg case "real(8)"
    target = 0.0d-00
    !$fcg case "complex(4)"
    target = (0.0e-00,0.0e-00)
    !$fcg case "complex(8)"
    target = (0.0d-00,0.0d-00)
    !$fcg case "logical"
    target = .false.
    !$fcg case "character(*)"
    target = ""
    !$fcg case default
    target = null
    !$fcg end switch
  end if

  return
end subroutine

!==============================================================================

!$fcg end generator

