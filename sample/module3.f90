!$fcg start define
nbyte:
  - 4
  - 8
!$fcg end define

module lib_axpy
  implicit none

  interface axpy
    !$fcg start do rtype in ${nbyte}
      !$fcg start do itype in ${nbyte}
    module procedure :: axpy_r${rtype}i${itype}
      !$fcg end do
    !$fcg end do
  end interface

  contains
    !$fcg start generator
    !$fcg start parameterize
    rtype: ${nbyte}
    itype: ${nbyte}
    !$fcg end parameterize
    
    subroutine axpy_r${rtype}i${itype}(n,c,a,inca,b,incb)
      implicit none
      integer(${itype}), intent(in)    :: n
      integer(${itype}), intent(in)    :: inca,incb
      real(${rtype}),    intent(in)    :: c
      real(${rtype}),    intent(in)    :: a(inca*n)
      real(${rtype}),    intent(inout) :: b(incb*n)
      !-------------------------------
      integer(${itype}) :: i, ia, ib
      
      do i=1,n
        ia = (i-1) * inca
        ib = (i-1) * incb
        b(ib) = b(ib) + c * a(ia)
      end do
      
      return
    end subroutine
    
    !--------------------------------------------------------------------------
    !$fcg end generator

end module
