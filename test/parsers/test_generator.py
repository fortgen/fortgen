###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy
import re

from source import parse
from source.define_keyword import key
from source.define_keyword import sec 
import parsers._generator_ 

class Test_parsers_generator():
	pattern_generator = []
	pattern_generator.append(
		(
			[
				"!$fcg start generator",
				"real(8) function one()",
				"  implicit none",
				"  ",
				"  return 1.0e-00 ",
				"end function",
				"!$fcg end generator",
			]
			,
			[
				"real(8) function one()",
				"  implicit none",
				"  ",
				"  return 1.0e-00 ",
				"end function",
			]
		)
	)


	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_generator)
	def test_generator(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		wrk = parsers._generator_.wrk()
		for data in data_in[1:] :
			args = re.split(r"[ ]+",data)
			if re.match(re.escape(key.prefix), args[0], flags=re.IGNORECASE) :
				dict_out = wrk.finalize(dict_out)
			else :
				dict_out = wrk.input(dict_out,data)
			continue
		
		result = dict_out[sec._source_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del wrk
		del dict_out
		return

	@pytest.mark.parametrize("data_in,expected",pattern_generator)
	def test_parse(self,data_in,expected):
		dict_out = parse._toDict(data_in)
		result = dict_out[sec._source_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return





