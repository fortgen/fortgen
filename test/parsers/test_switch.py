###
### Loading default python modules
###
import os
import sys
import pathlib
import pytest
import copy
import re

from source.define_keyword import key
from source.define_keyword import sec 
from source import parse 
import parsers._case_ 
import parsers._switch_ 

class Test_parsers_switch():
	pattern_switch = []
	pattern_switch.append(
		(
			[
				"!$fcg start switch ${x} ",
				"!$fcg case > 1.0",
				"x = 1.0e-00",
				"!$fcg case <= 0.5",
				"x = 0.5e-00",
				"!$fcg case default",
				"x = x * 1.1",
				"!$fcg end switch",
			]
			,
			[
				[key._elif_,"${x} > 1.0","x = 1.0e-00"],
				[key._elif_,"${x} <= 0.5","x = 0.5e-00"],
				[key._else_,None,"x = x * 1.1"],
			]
		)
	)
	
	pattern_case_getFormula = []
	pattern_case_getFormula.append(
		(
			"1"
			,
			"== 1"
		)
	)
	pattern_case_getFormula.append(
		(
			"[1,2,3,5,9]"
			,
			"in [1,2,3,5,9]"
		)
	)
	pattern_case_getFormula.append(
		(
			" in [1,2,3,5,9] "
			,
			"in [1,2,3,5,9]"
		)
	)
	pattern_case_getFormula.append(
		(
			" >= 1.0 "
			,
			">= 1.0"
		)
	)
	pattern_case_getFormula.append(
		(
			" < 1.0 "
			,
			"< 1.0"
		)
	)
	pattern_case_getFormula.append(
		(
			" != 1.0 "
			,
			"!= 1.0"
		)
	)
	
	#####
	#####
	@classmethod
	def setup_class(cls):
		return
	
	@classmethod
	def teardown_class(cls):
		return
	
	def setup_method(self,method):
		return
	
	def teardown_method(self,method):
		return
	
	#####
	#####
	@pytest.mark.parametrize("data_in,expected",pattern_switch)
	def test_switch(self,data_in,expected):
		dict_out = copy.deepcopy(sec.template)
		wrk = None 
		target = " ".join(re.split(r"[ ]+",data_in[0])[3:])
		for line in data_in[1:]:
			args = re.split(r"[ ]+",line)
			if re.match(re.escape(key.prefix), args[0], flags=re.IGNORECASE) :
				del args[0]
				if wrk is not None:
					dict_out = wrk.finalize(dict_out)
				del wrk
				del args[0]
				wrk = parsers._case_.wrk(target," ".join(args))
			else :
				wrk.input(line)
				
			continue
		
		result = dict_out[sec._source_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del wrk
		del dict_out
		return

	@pytest.mark.parametrize("data_in,expected",pattern_switch)
	def test_parse(self,data_in,expected):
		dict_out = parse._toDict(data_in)
		result = dict_out[sec._source_]
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		del dict_out
		return
	
	@pytest.mark.parametrize("data_in,expected",pattern_case_getFormula)
	def test_case_getFormula(self,data_in,expected):
		result = parsers._case_._getFormula(data_in)
		print(data_in)
		print(result)
		print(expected)
		assert result == expected
		return



