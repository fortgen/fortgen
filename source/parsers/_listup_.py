###
###
###
import re
import sys
import copy
import pathlib

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template
import parsers._parameterize_
import argument


keywords = [
	key._do_,
	key._at_,
	key._format_,
	key._delimiter_,
]

class wrk(template):
	_name_keyword = key._listup_
	
	_delimiter_default = ","
	
	def __init__(self,line0):
		args0 = re.split(r"[ ]", line0) 
		# [Todo] "at" or "for" included string in iterative variable arguments.  
		args = [ 
			key.toSystemKeyword(i) if key.toSystemKeyword(i) is not None
			else i
			for i in args0
		]
		
		### Analyze arguments ###
		dict_args = argument.get(" ".join(args),keywords)
		
		### Set "delimiter" block ###
		_delimiter = dict_args.get(key._delimiter_)
		if _delimiter is None :
			_delimiter = self._delimiter_default
		else :
			if re.match('".*"',_delimiter) or re.match("'.*'",_delimiter) :
				_delimiter = _delimiter[1:-1]
			else :
				pass
		
		### Set "for" block ###
		_parameter =  dict_args.get(key._do_)
		if _parameter is not None :
			_param = re.split(r"[ ]",_parameter)[0]
			if "=" in _param:
				_param0 = _param.split("=")
				_param  = _param0[0]
			else :
				pass
		else :
			_param = None
		
		### Set "at" block ###
		_param_at = dict_args.get(key._at_)
		if _param_at is None :
			_param_at = _param
		
		if _param_at is None :
			raise Exception("[ERROR] Missing variable for position indicator of 'at'.")
		
		### Set "format" block ###
		_format = dict_args.get(key._format_)
		if _format is None:
			_format = dict_args["*"]
		
		if _format is None :
			if _param is None :
				raise Exception("[ERROR] Output format is necessary when using multiple parameterization.")
			else :
				_format = "${" + _param + "}"
		
		### Store information ###
		self._parameter = _parameter
		self._storage   = []
		self._format    = _format
		self._position  = "@{" + _param_at + "}"
		self._delimiter_mark = "#{" + _param_at + "}"
		self._delimiter = _delimiter
		return
	
	def input(self,line):
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		if self._parameter is None:
			pass
		else :
			dict_out = parsers._parameterize_.inline(dict_out,self._parameter)
		
		dict_out[sec._task_] = key._listup_
		dict_out[sec._source_].extend(self._storage)
		dict_out[sec._define_][self._position] = self._format
		dict_out[sec._define_][self._delimiter_mark] = self._delimiter
		return dict_out
	
	def __del__(self):
		del self._parameter
		del self._storage
		del self._format
		del self._position
		return
	





