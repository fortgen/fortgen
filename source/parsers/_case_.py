###
###
###
import re
import sys
import copy
import pathlib
import yaml

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template

class wrk(template) :
	_name_keyword = "case"
	
	def __init__(self,target,line,isFirst=False):
		self._target = target.strip()
		
		args = re.split(r"[ ]+",line.strip()) 
		isDefault = key.toSystemKeyword(args[0])
		if isDefault is not None and isDefault == key._default_ :
			self._storage = [ key._else_, None, ]
		else :
			eqs = self._target + " " + _getFormula(line)
			if isFirst :
				self._storage = [ key._if_, eqs, ]
			else :
				self._storage = [ key._elif_, eqs, ]
		
		return
	
	def input(self,line):
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		dict_out[sec._source_].append(self._storage)
		return dict_out
	
	def __del__(self):
		del self._storage
		return


#####
def _getFormula(block0):
	# >=, <= < > !=
	# in []
	
	block = " ".join(re.split(r"[ ]+",block0)).strip() 
	if re.match(r"^\[.+\]",block):
		return "in " + block
	elif re.match(r"^in \[.+\]",block):
		return block
	elif re.match(r"^[\>\=\<\!]",block):
		return block
	else :
		return "== " + block
	return 




