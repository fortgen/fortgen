###
###
###
import re
import sys
import copy
import pathlib
import yaml

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template



class wrk(template):
	_name_keyword = key._else_
	
	def __init__(self,line):
		self._storage = [key._else_,None]
		return
	
	def input(self,line):
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		if len(self._storage) == 2:
			self._storage.append(None)
		dict_out[sec._source_].append(self._storage)
		return dict_out
	
	def __del__(self):
		del self._storage
		return
	


#####
def inline(dict_out,line):
	raise Exception("[ERROR] Inline 'else' statement is not implemented.")
	return
