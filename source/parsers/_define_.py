###
###
###
import re
import sys
import copy
import pathlib
import yaml

###
###
###
from define_keyword import key
from define_keyword import sec
from template import template



class wrk(template) :
	_name_keyword = key._define_
	
	def __init__(self):
		self._storage = []
		return
	
	def input(self,line):
		self._storage.append(line)
		return
	
	def finalize(self,dict_out):
		text_yaml = "\n".join(self._storage)
		dictlist_t = yaml.safe_load(text_yaml)
		dict_out[sec._define_].update(dictlist_t)
		return dict_out
	
	def __del__(self):
		del self._storage
		return
	

#####
def inline(dict_out,line):
	# FORMAT: i = [1,2,3,4], T = ["integer(4)","integer(8)"]
	# FORMAT: idict = { "integer(4)" : "i4","integer(8)" : "i8", }
	args = re.split(r"[=]+",line)
	valname = args[0].strip()
	del args
	val_str = re.sub(valname + "[ ]+=[ ]+","",line)
	
	if re.search(re.escape("${") + ".+" + re.escape("}"),val_str):
		vals = val_str
	else :
		vals = eval(val_str)
		if type(vals) == type(range(0,1)) :
			vals = [ i for i in vals ]
	
	dict_t = { valname : vals }
	dict_out[sec._define_].update(dict_t)
	return dict_out



