import sys
import pathlib

rootdir = pathlib.Path(__file__).resolve().parent
sys.path.append(str(rootdir) + "/source")

